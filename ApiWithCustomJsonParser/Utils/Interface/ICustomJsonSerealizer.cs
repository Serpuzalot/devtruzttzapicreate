﻿

using ApiWithCustomJsonParser.Data.Models;

namespace ApiWithCustomJsonParser
{
    public interface ICustomJsonSerealizer
    {
        public string Serealize<T>(List<T> units, T obj);

        public T Deserializer<T>(string jsonData, T obj, dynamic? interiorClass);

    }
}
