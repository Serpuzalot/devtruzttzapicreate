﻿using Microsoft.AspNetCore.Mvc;

namespace ApiWithCustomJsonParser.Models
{
    public class GetAllRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string City { get; set; }

    }
}
