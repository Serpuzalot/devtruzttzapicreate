using ApiWithCustomJsonParser.Data.Models;

namespace ApiWithCustomJsonParser.Data.Services
{
    public class AddressService : Address
    {
        
        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Address p = (Address)obj;
                return (p.City == City) && (AddressLine == p.AddressLine);
            }
        }
    }
}
