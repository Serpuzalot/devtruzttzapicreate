﻿using ApiWithCustomJsonParser.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiWithCustomJsonParser.Data;

public class ApplicationContext:DbContext 
{
    public DbSet<Person> Persons { get; set; } = null!;
    public DbSet<Address> Addresses { get; set; } = null!;
    public ApplicationContext(DbContextOptions<ApplicationContext> options)
        : base(options)
    {
        Database.EnsureCreated();
            
    }


}