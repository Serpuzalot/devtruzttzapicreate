﻿using System.Linq.Expressions;
using System.Reflection;
using ApiWithCustomJsonParser.Data;
using ApiWithCustomJsonParser.Data.Models;
using ApiWithCustomJsonParser.Data.Services;
using Microsoft.AspNetCore.Mvc;

namespace ApiWithCustomJsonParser.Controllers;

[ApiController]
[Route("[controller]")]
public class MainController : ControllerBase
{
    private ApplicationContext db;
    private readonly ICustomJsonSerealizer _jsonSerealizer;
    public MainController(ApplicationContext db, ICustomJsonSerealizer serealizer)
    {
        this.db = db;
        _jsonSerealizer = serealizer;
    }

    /// <summary>
    /// Get persons from db by city/lastName/firstName
    /// </summary>
    /// <param name="json"></param>
    /// <returns>Get Persons by city </returns>
    /// <remarks>
    /// Example request:
    ///
    ///     GET /GetAll
    ///     {
    ///       city: "kiev"
    ///     }
    ///     
    /// Second example request:
    /// {
    /// 
    /// }
    /// </remarks>
    [HttpGet("GetAll")]
    public IActionResult GetAll(string json)
    {
        GetAllRequestService service = new GetAllRequestService();
        string result = service.GetAll(json, db, _jsonSerealizer).Result;
        return Ok(result);
    }


    /// <summary>
    /// Add new Person to db or update exist 
    /// </summary>
    /// <param name="jsonInput"></param>
    /// <returns>A newly created Person </returns>
    /// <remarks>
    /// Example request:
    ///
    ///     POST /Save
    ///     {
    ///        firstName: "Ivan",
    ///        lastName: "Petrov",
    ///        address: {
    ///                 city: "Kiev",
    ///                 addressLine: prospect "Peremogy" 28/7,
    ///             }
    ///
    ///     }
    ///
    /// </remarks>
    [HttpPost("Save")]
        public JsonResult Save(string jsonInput)
        {
            PersonService pService =  new PersonService();
        string result = "";
            Task.Run(async () => {
                result = await pService.Save(jsonInput,db,_jsonSerealizer);
            }).Wait();
        return new JsonResult(result);
        
        }


    [HttpPut]
        private JsonResult Update(Person dbPerson,Person updatedPerson)
        {
            dbPerson.FirstName = updatedPerson.FirstName;
            dbPerson.LastName = updatedPerson.LastName;
            dbPerson.Address = updatedPerson.Address;
            db.Persons.Update(dbPerson);
            db.SaveChanges();
            return new JsonResult($"Updated object in data base : {dbPerson.Id}");
        }
}
