﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiWithCustomJsonParser.Data.Models;
using ApiWithCustomJsonParser.Data.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Extensions;
using Dynamitey;

namespace ApiWithCustomJsonParser
{
    public class CustomJsonSerealizer : ICustomJsonSerealizer
    {
        private Dictionary<string,string> _propertysShortLongForm = new Dictionary<string, string>();

        private void GetPropertys<T>()
        {
            var propsArray = typeof(T).GetProperties();
            foreach(var prop in propsArray)
            {
                var a =    prop.PropertyType;
                if (!a.FullName.Contains("System") && a.GetProperties().Any())
                {
                    _propertysShortLongForm.Add(prop.Name,prop.Name);
                    foreach (var el in a.GetProperties())
                    {
                        if (!el.Name.Contains("Id") && !el.Name.Contains("ID") && !el.Name.Contains("id"))
                        {
                            _propertysShortLongForm.Add(el.Name,$"{prop.Name}.{el.Name}");
                        }
                    }
                    continue;
                }
                if(!prop.Name.Contains("Id") && !prop.Name.Contains("ID") && !prop.Name.Contains("id"))
                {
                    _propertysShortLongForm.Add(prop.Name,"");
                }
               

            }
        }

        private Dictionary<string,string> FillDictionary(string name,string  value,Dictionary<string,string> dict)
        {
            foreach (var el in _propertysShortLongForm.Keys)
            {
                
                string[] nameArray = name.Trim().Split('"','{');
                foreach (string str in nameArray)
                {
                    if (!String.IsNullOrEmpty(str) && str != "{")
                    {
                        if (el.ToLower() == str.Trim().ToLower() && el != _propertysShortLongForm[el])
                        {
                            dict[el] = value;
                            break;
                        }
                    }
                }
            }
            return dict;
        }

        public string Serealize<T>(List<T> units,T obj)
        {
            _propertysShortLongForm = new Dictionary<string, string>();
            GetPropertys<T>();
            StringBuilder jsonBuilder = new StringBuilder();
            string quote = "\"";
            int counter = 0;
            bool checkBracket = false;
            jsonBuilder.Append("[\n");
            if (units.Count < 1)
            {
                return "{}";
            }
            for (int i = 0; i < units.Count; i++)
            {
                dynamic unit = units[i];
                string a = _propertysShortLongForm.Keys.Where(k => _propertysShortLongForm[k] == k).First();
                dynamic address = Dynamic.InvokeGet(unit, a);
                jsonBuilder.Append("{");
                counter = 0;
                foreach (var el in _propertysShortLongForm.Keys)
                {
                    if (el == _propertysShortLongForm[el])
                    {
                        jsonBuilder.Append($"{quote}{el}{quote} : {"{"}");
                        counter++;
                        continue;
                    }
                    else if (_propertysShortLongForm[el] != "")
                    {
                        checkBracket = true;
                        jsonBuilder.Append($"{quote}{el}{quote} : {quote}{Dynamic.InvokeGet(address, el)}{quote}");

                    }
                    else
                    {
                        if (checkBracket)
                        {
                            jsonBuilder.Append("}\n");
                            checkBracket = false;
                        }    
                        jsonBuilder.Append($"{quote}{el}{quote} : {quote}{Dynamic.InvokeGet(unit, el)}{quote}");
                    }
                    if (counter != _propertysShortLongForm.Count-1)
                    {
                        jsonBuilder.Append(",");
                    }
                    jsonBuilder.Append('\n');
                    counter++;
                }
                if (checkBracket)
                {
                    jsonBuilder.Append("}\n");
                    checkBracket = false;
                }
                jsonBuilder.Append("}");
                if (i != units.Count - 1)
                {
                    jsonBuilder.Append(",\n");
                }
              
            }
            jsonBuilder.Append("]\n");
            return jsonBuilder.ToString();
           
        }

        public T Deserializer<T>(string jsonData,T obj,dynamic? interiorClass)
        {
            _propertysShortLongForm = new Dictionary<string, string>();
            GetPropertys<T>();
            Dictionary<string, string> nameAndValues = new Dictionary<string, string>();
            foreach(var el in _propertysShortLongForm.Keys)
            {
                if(el != _propertysShortLongForm[el])
                {
                    nameAndValues.Add(el, "");
                }
            }
            string[] jsonObjects = jsonData.Split(',');
            foreach(var str in jsonObjects)
            {
                string[] jsonFilds = str.Split(':');
                if(jsonFilds.Length == 3)
                {
                    string jsonName = jsonFilds[1];
                    string jsonValue = jsonFilds[2];
                    jsonFilds = new string[] { jsonName,jsonValue };
                }
                if (jsonFilds.Length > 1)
                {
                    nameAndValues = FillDictionary(jsonFilds[0], jsonFilds[1], nameAndValues);
                }
               

            }

            T result = GetFilledCollection<T>(nameAndValues, obj,interiorClass);
            
            return result;
        }

        private string RemoveQuotes(string value)
        {
            string[] valueParts = value.Split('"', '{', '}');
            StringBuilder sb = new StringBuilder();
            foreach (string part in valueParts)
            {
                if (!String.IsNullOrEmpty(part.Trim()) && part != "{" && part != "}")
                {
                    sb.Append(part);
                }
            }
            return sb.ToString();
        }

        private T GetFilledCollection<T>(Dictionary<string, string> values,T obj,dynamic interiorClass)
        {
            dynamic usedClass = obj;
            string interiorClassName = "";
            foreach(var el in _propertysShortLongForm.Keys)
            {
                if(el == _propertysShortLongForm[el])
                {
                    interiorClassName = el;
                    break;
                }
            }
            foreach (var el in values)
            {
                if(interiorClass != null)
                {
                    if (_propertysShortLongForm[el.Key] != "" && !_propertysShortLongForm.Keys.Contains(_propertysShortLongForm[el.Key]))
                    {
                        Dynamic.InvokeSet(interiorClass, el.Key, RemoveQuotes(el.Value));
                        continue;
                    }
                }
                
                Dynamic.InvokeSet(usedClass, el.Key, RemoveQuotes(el.Value));
            }

            if(interiorClass != null)
            {
                Dynamic.InvokeSet(usedClass, interiorClassName, interiorClass);
            }
            return usedClass;


        }

    }
}
