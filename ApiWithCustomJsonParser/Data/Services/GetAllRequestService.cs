﻿using ApiWithCustomJsonParser.Data.Models;
using ApiWithCustomJsonParser.Models;
using System.Text;

namespace ApiWithCustomJsonParser.Data.Services
{
    public class GetAllRequestService : GetAllRequest
    {
        public Task<string> GetAll(string request,ApplicationContext db,ICustomJsonSerealizer serealizer)
        {
            // get Persons entities from database
            // filter by GetAllRequest fields (null or empty fields should be ignored)
            // use your own manually written json serializer to serialize result into string
            db.Addresses.ToList();
            List<Person> persons = db.Persons.ToList();
            List<PersonService> dbPersons = new List<PersonService>();
            GetAllRequestService inputRequest = serealizer.Deserializer<GetAllRequestService>(request, new GetAllRequestService(), null) ;
            foreach(Person dbPerson in persons)
            {
                PersonService person = new PersonService();
                person.Id = dbPerson.Id;
                person.FirstName = dbPerson.FirstName;
                person.LastName = dbPerson.LastName;
                person.Address = dbPerson.Address;
                person.AddressId = dbPerson.AddressId;
                dbPersons.Add(person);
            }
            List<PersonService> result = new List<PersonService>();
            foreach(var el in dbPersons)
            {
                if (!String.IsNullOrEmpty(inputRequest.FirstName))
                {
                    if(el.FirstName.ToLower() == inputRequest.FirstName.ToLower())
                    {
                        result.Add(el);
                        continue;
                    }
                }
                if (!String.IsNullOrEmpty(inputRequest.LastName))
                {
                    if(el.LastName.ToLower() == inputRequest.LastName.ToLower())
                    {
                        result.Add(el);
                        continue;
                    }
                }
                if (!String.IsNullOrEmpty(inputRequest.City))
                {
                    if(el.Address.City.ToLower() == inputRequest.City.ToLower())
                    {
                        result.Add(el);
                        continue;
                    }
                }
            }

            if (!result.Any())
            {

                return Task.Run(() => serealizer.Serealize<PersonService>(dbPersons, new PersonService()));
            }
            return Task.Run(() => serealizer.Serealize<PersonService>(result, new PersonService()));
        }

       
    }
}
