﻿using ApiWithCustomJsonParser.Data.Models;
using System.Text;

namespace ApiWithCustomJsonParser.Data.Services;

public class PersonService : Person
{
    public Task<string> Save(string jsonInput, ApplicationContext db,ICustomJsonSerealizer serealizer)
    {
        db.Addresses.ToList();
        dynamic address = new Address();
        PersonService person = serealizer.Deserializer<PersonService>(jsonInput, new PersonService(), address );
        if (String.IsNullOrEmpty(person.LastName))
        {
            return Task.Run(() => "Error!");
        }
        Person searchIdentyBySurname = db.Persons.FirstOrDefault(x => x.LastName == person.LastName && x.FirstName == person.FirstName);

        if (searchIdentyBySurname !=null )
        {
            searchIdentyBySurname.FirstName = person.FirstName;
            searchIdentyBySurname.LastName = person.LastName;
            if (!searchIdentyBySurname.Address.Equals(person.Address))
            {
                db.Addresses.Remove(searchIdentyBySurname.Address);
                searchIdentyBySurname.Address = person.Address;
            }
            db.Persons.Update(searchIdentyBySurname);
            db.SaveChanges();
            return Task.Run(() => $"Updated object with id: {searchIdentyBySurname.Id.ToString()}");
        }
        else
        {
            db.Persons.Add(person);
            db.SaveChanges();

            return Task.Run(() => $"Add new object in data base with id : {db.Persons.ToList().Last().Id} ");
        }
    }

}